<?php 

class ti3
{
	private $host = "localhost";
	private $user = "root";
	private $db = "db_ti3";
	private $pass = "";
	protected $conn;

	public function __construct()
	{
		$this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->db,$this->user,$this->pass);
	}

	public function showData($query){
		// $sql = "SELECT * FROM $table";
		$q = $this->conn->query($query) or die("failed!");
		while ($r = $q->fetch(PDO::FETCH_ASSOC)) {
			$data[]=$r;
		}
		return $data;
	}

	public function getData($id,$table,$key)
	{
		$stmt = $this->conn->prepare("SELECT * FROM $table WHERE $key=:key");
		$stmt->execute(array(":key"=>$id));
		$editRow=$stmt->fetch(PDO::FETCH_ASSOC);
		return $editRow;
	}

	public function paging($query,$records_per_page)
	{
		$starting_position=0;
		if (isset($_GET["page_no"])) {
			$starting_position=($_GET["page_no"]-1)*$records_per_page;
		}
		$query2=$query." limit $starting_position,$records_per_page";
		return $query2;
	}

	public function paginglink($query,$records_per_page){
		$self = $_SERVER['PHP_SELF'];
		if ($_GET['page']==$_GET['page']) {
			$page = $_GET['page'];
		}

		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		$total_no_of_records = $stmt->rowCount();

		if ($total_no_of_records > 0) {
			?><ul class="pagination"><?php
			$total_no_of_pages=ceil($total_no_of_records/$records_per_page);
			$current_page=1;

			if (isset($_GET["page_no"])) {
				$current_page=$_GET["page_no"];
			}

			if ($current_page!=1) {
				$previous = $current_page-1;
				echo "<li><a href='".$self."?page=".$page."&page_no=1'>First</a></li>";
				echo "<li><a href='".$self."?page=".$page."&page_no=".$previous."'>First</a></li>";
			}

			for ($i=1; $i<=$total_no_of_pages; $i++) { 
				if ($i==$current_page) {
					echo "<li><a href='".$self."?page=".$page."&page_no=".$i."' style='color:red;'>".$i."</a></li>";
				}else{
					echo "<li><a href='".$self."?page=".$page."&page_no=".$i."'>".$i."</a></li>";
				}
			}

			if ($current_page!=$total_no_of_pages) {
				$next=$current_page+1;
				echo "<li><a href='".$self."?page=".$page."&page_no=".$next."'>Next</a></li>";
				echo "<li><a href='".$self."?page=".$page."&page_no=".$total_no_of_pages."'>Last</a></li>";
			}
		}

	}

	public function jumlah($query){
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$row = $stmt->rowCount();
		print($row);
	}

	public function search($query){
		// $sql = "SELECT * FROM $table";
		$q = $this->conn->query($query) or die("failed!");
		while ($r = $q->fetch(PDO::FETCH_ASSOC)) {
			$data[]=$r;
		}
		return $data;
	}

	public function delete($id,$tabel,$key){
		$stmt = $this->conn->prepare("DELETE FROM $tabel WHERE $key=:id");
		$stmt->bindparam(":id",$id);
		$stmt->execute();
		return true;
	}
}

class user extends ti3{
	public function create($nama,$username,$password,$email,$level){
		try {
		$stmt = $this->conn->prepare('INSERT INTO tbl_user(nama,username,password,email,level) VALUES(?,?,?,?,?)');
		$stmt->bindParam(1,$nama);
		$stmt->bindParam(2,$username);
		$stmt->bindParam(3,$password);
		$stmt->bindParam(4,$email);
		$stmt->bindParam(5,$level);
		$stmt->execute();
		
		return true;	
		} catch (PDOException $e) {
			return false;
		}
	}

	public function update($username,$password,$email){
		try {
			$stmt = $this->conn->prepare("UPDATE tbl_user SET password=:password WHERE username=:username OR email=:email");
	    $stmt->bindparam(":username",$username);
	    $stmt->bindparam(":password",$password);
	    $stmt->bindparam(":email",$email);
	    $stmt->execute();

    	return true;
	  } catch (PDOException $e) {
	    echo $e->getMessage();
		  return false;
	  }
	}
}

class lapangan extends ti3{

	public function create($nama,$nama_lapangan,$tanggal,$jam,$durasi,$no_telp,$harga_lapangan,$total_biaya,$Status){
		try {
		$stmt = $this->conn->prepare('INSERT INTO tbl_transaksi(nama,nama_lapangan,tanggal,jam,durasi,no_telp,harga_lapangan,total_biaya,Status) VALUES(?,?,?,?,?,?,?,?,?)');
		$stmt->bindParam(1,$nama);
		$stmt->bindParam(2,$nama_lapangan);
		$stmt->bindParam(3,$tanggal);
		$stmt->bindParam(4,$jam);
		$stmt->bindParam(5,$durasi);
		$stmt->bindParam(6,$no_telp);
		$stmt->bindParam(7,$harga_lapangan);
		$stmt->bindParam(8,$total_biaya);
		$stmt->bindParam(9,$Status);
		$stmt->execute();
		
		return true;	
		} catch (PDOException $e) {
			return false;
		}
	}

}

class transaksi extends ti3{

	public function t_transaksi($nama,$nama_lapangan,$tanggal,$jam,$durasi,$no_telp,$harga_lapangan,$total_biaya,$Status){
		try {
		$stmt = $this->conn->prepare('INSERT INTO tbl_transaksi(nama,nama_lapangan,tanggal,jam,durasi,no_telp,harga_lapangan,total_biaya,Status) VALUES(?,?,?,?,?,?,?,?,?)');
		$stmt->bindParam(1,$nama);
		$stmt->bindParam(2,$nama_lapangan);
		$stmt->bindParam(3,$tanggal);
		$stmt->bindParam(4,$jam);
		$stmt->bindParam(5,$durasi);
		$stmt->bindParam(6,$no_telp);
		$stmt->bindParam(7,$harga_lapangan);
		$stmt->bindParam(8,$total_biaya);
		$stmt->bindParam(9,$Status);
		$stmt->execute();
		
		return true;	
		} catch (PDOException $e) {
			return false;
		}
	}
	
}

?>