<?php 
include_once('inc/class.ti3.php');
$user = new user;

if (isset($_POST['btn-save'])) {
	$nama = $_POST['nama'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$password = md5($password);
	$email = $_POST['email'];
	$level = $_POST['level'];
    //  proses simpan ke database
    if ($user->create($nama,$username,$password,$email,$level)) {
        header('location: login.php?msg=success');
    }	
}
?>
<!DOCTYPE html>
<html lang="en">
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/logo.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">

    <title> Pendaftaran | futsal booking</title>
    <style>
        body{
            background-image: url(img/bg1.png);
            background-size: cover;
        }
    </style>
  </head>
  <body>

    <div class="container">
        <div class="row">
            <div class="mt-3">
                <img src="img/logo1.png" width="240" alt="">
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-7 mt-5">
                <img src="img/Logo2.png" width="500" alt=""> 
            </div>
            <div class="col-md-5 mt-5">
                <div class="card shadow p-3 border-radius-20 border-0">
                    <div class="card-body">
                        <h5>Pendaftaran</h5>
                    <div class="container">
                        <div class="row">
                            <div class="ml-auto">
                                <small>
                                    Sudah punya akun?
                                    <a href="login.php" class="text-decoration-none text-theme">Masuk</a>
                                </small>
                            </div>
                        </div>
                    </div>
                    <form method="post" enctype="multipart/form-data" action="" class="mt-4">
                        <div class="form-group">
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap.." Required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Username.." Required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password.." Required>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Email.." Required>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="level" style="width: 375px">
                            <option value="" disabled selected hidden>Silahkan Pilih User..</option>
                            <option value="user">User</option>
                        </select>
                        </div>
                        <button type="submit" class="btn btn-color-theme btn-block p-2 mt-3" name="btn-save">Daftar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

   
  </body>
</html>