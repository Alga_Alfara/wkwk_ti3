<!doctype html>
<html lang="en">
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/logo.png" type="image/x-icon">
    <link rel="stylesheet" href="../css/style.css">

    <title> Lapangan Admin | futsal booking</title>
    <style>
        body{
            background-image: url(../img/bg2.png);
            background-size: cover;
        }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light p-4">
        <div class="container">
            <div class="navbar-brand">
                <img src="../img/logo1.png" width="240" alt="">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item p">
                        <a href="home.php" class="text-decoration-none text-home">Home</a>
                    </li>
                    <li class="nav-item p">
                        <a href="lapangan.php" class="text-decoration-none text-home">Lapangan</a>
                    </li>
                    <li class="nav-item p">
                        <a href="pesanan.php" class="text-decoration-none text-home">Pesanan</a>
                    </li>
                    <li class="nav-item p">
                        <a href="../login.php" class="text-decoration-none text-home">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section class="hero container">
        <div class="mt-2">
            <div class="row">
                <div class="col-md-5">
                    <h4 class="font-weight-bold">Data Lapangan Admin</h4>
                </div>
            </div>
        </div>
        <div style="padding-top: 10px" class="panel-body">
		<br>
    <?php 
    include_once('../inc/class.ti3.php');
		if (isset($_GET['msg'])) {
			if ($_GET['msg']=="success") {
				$msg="
				<div class='alert alert-success'>
    		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    		<strong>Success!</strong> Data berhasil di tambah.
  			</div>
				";
			}
            // elseif ($_GET['msg']=="delete") {
			// 	$msg="
			// 	<div class=\"alert alert-danger\">
    		// <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
    		// <strong>Success!</strong> Data berhasil di hapus.
  			// </div>
			// 	";
			// }
		}

		if (isset($msg)) {
			echo $msg;
		}
		?>
        </div>
        
        <table class="table table-bordered">
            <thead>
                <tr>
					<th style="text-align:center;" width="5%">No</th>
					<th style="text-align:center;">Nama Lapangan</th>
					<th style="text-align:center;">Harga</th>
					<th style="text-align:center;">foto</th>
				</tr>
            </thead>
            <tbody>
				<?php 
				include_once('../inc/class.ti3.php');
                $lapangan = new lapangan;
                $records_per_page=5;
				$query = "SELECT * FROM tbl_lapangan";
                $newquery = $lapangan->paging($query,$records_per_page);
                // penomoran halaman data pada halaman
				if (isset($_GET['page_no'])) {
				$page = $_GET['page_no'];
				}
				if (empty($page)) {
					$posisi = 0;
					$halaman = 1;
				}else{
					$posisi = ($page - 1) * $records_per_page;
				}
				$no=1+$posisi;
				foreach ($lapangan->showData($newquery) as $value) {
					?>
					<tr style="text-align: center;">
					<td><?php echo $no; ?></td>
					<td><?=$value['nama_lapangan']; ?></td>
					<td><?=$value['harga']; ?></td>
					<td><img src="../img/<?=$value['foto']?>" height="50px" width="50px"></td>
					</tr>
					<?php
                    $no++;
				}
				?>
                </tbody>
        </table>
        Jumlah : <b><?php $lapangan->jumlah($query); ?> Lapangan</b>
    </section>
    
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

   
  </body>
</html>