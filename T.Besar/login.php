<!DOCTYPE html>
<html>
    <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/logo.png">
    <link rel="stylesheet" href="css/style.css">

    <title> Login | futsal booking</title>
    <style>
        body{
            background-image: url(img/bg1.png);
            background-size: cover;
        }
    </style>
  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="mt-3">
                <img src="img/logo1.png" width="240" alt="">
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-7 mt-5">
                <img src="img/Logo2.png" width="500" alt=""> 
            </div>

            <div style="padding-top: 10px" class="panel-body">
            <!-- <br> -->
            <?php 
            if (isset($_GET['msg'])) {
                if ($_GET['msg']=="success") {
				$msg="
                <div class='alert alert-success'>
    		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
    		<strong>Success!</strong> Data berhasil di tambah.
            </div>
				";
			}elseif ($_GET['msg']=="edit") {
				$msg="
				<div class=\"alert alert-warning\">
    		<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
    		<strong>Success!</strong> Data berhasil di Edit.
  			</div>
				";
			}
		}

		if (isset($msg)) {
			echo $msg;
		}
		?>
        </div>
            <div class="col-md-5 mt-5">
                <div class="card shadow p-3 border-radius-20 border-0">
                    <div class="card-body">
                        <h5>Masuk</h5>
                        <form id="loginform" class="mt-4" role="form" action="cek-login.php" method="POST">
                        <div class="form-group">
                            <input id="login-email" type="text" class="form-control" name="email" placeholder="Email" required>
                        </div>
                        <div class="input-group">
                            <input id="login-password" type="password" class="form-control" name="password" placeholder="Kata Sandi" required>
                        </div>
                        <div class="container">
                            <div class="row mt-2">
                                <div>
                                    <small>
                                        Belum punya akun?
                                        <a href="pendaftaran.php" class="text-decoration-none text-theme">Daftar</a>
                                    </small>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-color-theme btn-block p-2 mt-3" name="submit" value="login" type="submit" >Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

   
  </body>
</html>