<!DOCTYPE html>
<html lang="en">
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="shortcut icon" href="img/logo.png" type="image/x-icon">
    <link rel="stylesheet" href="css/style.css">

    <title> Home | futsal booking</title>
    <style>
        body{
            background-image: url(img/bg2.png);
            background-size: cover;
        }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light p-4">
        <div class="container">
            <div class="navbar-brand">
                <img src="img/logo1.png" width="240" alt="">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item p">
                        <a href="index.php" class="text-decoration-none text-home">Home</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section class="hero container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-5">
                    <h5>Halo, Selamat Datang</h5>
                    <h2 class="font-weight-bold mt-3">Pesan lapangan futsal</h2>
                    <h2 class="font-weight-bold">Sekarang Juga</h2>
                    <h6 class="mt-3">Daftar Sekarang</h6>
                    <a href="pendaftaran.php" class="btn btn-color-theme pr-4 pl-4 pt-2 pb-2 mt-4">Daftar</a>
                    <a href="login.php" class="btn btn-outline-theme pr-4 pl-4 pt-2 pb-2 mt-4">Login</a>
                </div>
                <div class="col-md-7">
                    <div class="logo2">
                        <img src="img/Logo2.png" width="500" alt="">
                    </div>
                </div>
            </div>
        </div>

    </section>
    
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

   
  </body>
</html>