<?php 
include_once "../inc/class.ti3.php";
$lapangan = new lapangan;
?>

<!DOCTYPE html>
<html lang="en">
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/logo.png" type="image/x-icon">
    <link rel="stylesheet" href="../css/style.css">

    <title> Lapangan User | futsal booking</title>
    <style>
        body{
            background-image: url(../img/bg2.png);
            background-size: cover;
        }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light p-4">
        <div class="container">
            <div class="navbar-brand">
                <img src="../img/logo1.png" width="240" alt="">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item p">
                        <a href="home.php" class="text-decoration-none text-home">Home</a>
                    </li>
                    <li class="nav-item p">
                        <a href="lapangan.php" class="text-decoration-none text-home">Lapangan</a>
                    </li>
                    <li class="nav-item p">
                        <a href="pesanan.php" class="text-decoration-none text-home">Pesanan</a>
                    </li>
                    <li class="nav-item p">
                        <a href="../login.php" class="text-decoration-none text-home">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section class="hero container">
        <div class="mt-2">
            <div class="row">
                <div class="col-md-5">
                    <h2 class="font-weight-bold">Pilih lapangan futsal</h2>
                </div>
            </div>
        </div>
    </section>
    
    <form action="booking.php" method="post">
    <div class="mx-5 row m-1 px-5" style="background:green" >
        <div class="px-5">
            <div class="row my-5 text-center">
                <div class="col-auto m-2 card">
                    <div class="col-auto mt-3">
                        <div class="row-auto">
                            <h5 class="font-weight-bold">Lapangan 1</h5>
                            <?php 
                            $query="SELECT * FROM tbl_lapangan WHERE id=1";
                            foreach ($lapangan->showData($query) as $value) {
                                ?>
                            <img src="../img/<?=$value['foto']?>" class="card-img-top" style="width: 200px; height: 250px;" alt="...">
                            <?php
                            }
                            ?>
                        </div>
                        <div class="row-auto my-2">
                            <?php 
                            $query="SELECT * FROM tbl_lapangan WHERE id=1";
                            foreach ($lapangan->showData($query) as $value) {
							echo "<h6 class='font-weight-bold'>Rp.".$value['harga']."/Jam"."</h6>";
                            ?>
                            <a href="booking.php?id=<?=$value['id']?>" class="btn btn-color-theme btn-block p-2 mt-3">Booking</a>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-auto m-2 card">
                    <div class="col-auto mt-3">
                        <div class="row-auto">
                            <h5 class="font-weight-bold">Lapangan 2</h5>
                            <?php
                            $query="SELECT * FROM tbl_lapangan WHERE id=2";
                            foreach ($lapangan->showData($query) as $value) {
                                ?>
                            <img src="../img/<?=$value['foto']?>" class="card-img-top" style="width: 200px; height: 250px;" alt="...">
                            <?php
                            }
                            ?>
                        </div>
                        <div class="row-auto my-2">
                            <?php 
                            $query="SELECT * FROM tbl_lapangan WHERE id=2";
                            foreach ($lapangan->showData($query) as $value) {
							echo "<h6 class='font-weight-bold'>Rp.".$value['harga']."/Jam"."</h6>";
						?>
                            <a href="booking.php?id=<?=$value['id']?>" class="btn btn-color-theme btn-block p-2 mt-3">Booking</a>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-auto m-2 card">
                    <div class="col-auto mt-3">
                        <div class="row-auto">
                            <h5 class="font-weight-bold">Lapangan 3</h5>
                            <?php
                            $query="SELECT * FROM tbl_lapangan WHERE id=3";
                            foreach ($lapangan->showData($query) as $value) {
                                ?>
                            <img src="../img/<?=$value['foto']?>" class="card-img-top" style="width: 200px; height: 250px;" alt="...">
                            <?php
                            }
                            ?>
                        </div>
                        <div class="row-auto my-2">
                            <?php 
                            $query="SELECT * FROM tbl_lapangan WHERE id=3";
                            foreach ($lapangan->showData($query) as $value) {
							echo "<h6 class='font-weight-bold'>Rp.".$value['harga']."/Jam"."</h6>";
						?>
                            <a href="booking.php?id=<?=$value['id']?>" class="btn btn-color-theme btn-block p-2 mt-3">Booking</a>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
                <div class="col-auto m-2 card">
                    <div class="col-auto mt-3">
                        <div class="row-auto">
                            <h5 class="font-weight-bold">Lapangan 4</h5>
                            <?php
                            $query="SELECT * FROM tbl_lapangan WHERE id=4";
                            foreach ($lapangan->showData($query) as $value) {
                                ?>
                            <img src="../img/<?=$value['foto']?>" class="card-img-top" style="width: 200px; height: 250px;" alt="...">
                            <?php
                            }
                            ?>
                        </div>
                        <div class="row-auto my-2">
                            <?php 
                            $query="SELECT * FROM tbl_lapangan WHERE id=4";
                            foreach ($lapangan->showData($query) as $value) {
							echo"<h6 class='font-weight-bold'>Rp.".$value['harga']."/Jam"."</h6>";
						?>
                            <a href="booking.php?id=<?=$value['id']?>" class="btn btn-color-theme btn-block p-2 mt-3">Booking</a>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

   
  </body>
</html>