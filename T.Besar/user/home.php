<!doctype html>
<html lang="en">
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/logo.png" type="image/x-icon">
    <link rel="stylesheet" href="../css/style.css">

    <title> Home User | futsal booking</title>
    <style>
        body{
            background-image: url(../img/bg2.png);
            background-size: cover;
        }
    </style>
  </head>
  <body>
    <?php 
    session_start();
    if(!isset($_SESSION['nama'])|| $_SESSION['level'] != "user"){
        echo "<script>alert('Silahkan login terlebih dahulu')</script>";
        echo "<meta http-equiv='refresh' content='0; url=../login.php'>";
    }
    else{
    ?>
    <nav class="navbar navbar-expand-lg navbar-light p-4">
        <div class="container">
            <div class="navbar-brand">
                <img src="../img/logo1.png" width="240" alt="">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item p">
                        <a href="home.php" class="text-decoration-none text-home">Home</a>
                    </li>
                    <li class="nav-item p">
                        <a href="lapangan.php" class="text-decoration-none text-home">Lapangan</a>
                    </li>
                    <li class="nav-item p">
                        <a href="pesanan.php" class="text-decoration-none text-home">Pesanan</a>
                    </li>
                    <li class="nav-item p">
                        <a href="../Login.php" class="text-decoration-none text-home">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
 
    <section class="hero container">
        <div class="mt-5">
            <div class="row">
                <div class="col-md-5">
                    <h5>Hallo!, <?php echo $_SESSION['nama']; ?></h5>
                    <h2 class="font-weight-bold mt-3">Pesan Lapangan Futsal</h2>
                    <h2 class="font-weight-bold">Sekarang Juga</h2>
                </div>
                <div class="col-md-7">
                    <img src="../img/Logo2.png" width="500" alt="">
                </div>
            </div>
        </div>

    </section>
    
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

    <?php } ?>
  </body>
</html>