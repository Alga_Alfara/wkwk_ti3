<!-- Button-Booking Lapangan -->
<?php 
include_once('../inc/class.ti3.php');
$transaksi = new transaksi;
date_default_timezone_set('Asia/Jakarta');

if (isset($_POST['btn-save'])) {
    $id = $_POST['id'];
	$nama = $_POST['nama'];
    $nama_lapangan = $_POST['nama_lapangan'];
	$tanggal = $_POST['tanggal'];
	$jam = $_POST['jam'];
	$durasi = $_POST['durasi'];
	$no_telp = $_POST['no_telp'];
	$harga_lapangan = $_POST['harga_lapangan'];
	$total_biaya = $_POST['total_biaya'];
    $Status = $_POST['Status'];

    if ($transaksi->t_transaksi($nama,$nama_lapangan,$tanggal,$jam,$durasi,$no_telp,$harga_lapangan,$total_biaya,$Status)) {	
        header('location:pesanan.php?msg=success');
    }			
}
?>
<!DOCTYPE html>
<html lang="en">
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="shortcut icon" href="../img/logo.png" type="image/x-icon">
    <link rel="stylesheet" href="../css/style.css">

    <title> Booking | futsal booking</title>
    <style>
        body{
            background-image: url(../img/bg2.png);
            background-size: cover;
        }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light p-4">
        <div class="container">
            <div class="navbar-brand">
                <img src="../img/logo1.png" width="240" alt="">
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item p">
                        <a href="home.php" class="text-decoration-none text-home">Home</a>
                    </li>
                    <li class="nav-item p">
                        <a href="lapangan.php" class="text-decoration-none text-home">Lapangan</a>
                    </li>
                    <li class="nav-item p">
                        <a href="pesanan.php" class="text-decoration-none text-home">Pesanan</a>
                    </li>
                    <li class="nav-item p">
                        <a href="../login.php" class="text-decoration-none text-home">Log out</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
    <!-- Get Value User Login -->
    <?php 
    session_start();
    if(!isset($_SESSION['nama'])|| $_SESSION['level'] != "user"){
        echo "<script>alert('Silahkan login terlebih dahulu')</script>";
        echo "<meta http-equiv='refresh' content='0; url=../login.php'>";
    }
    else{
    ?>
    
    <!-- Get Value Page-Lapangan -->
    <?php 
    include_once '../inc/class.ti3.php';
    $lapangan = new lapangan;
    
    if (isset($_GET['id'])) {
	$id = $_GET['id'];
	extract($lapangan->getData($id,'tbl_lapangan','id'));
}
?>

<?php 
if (isset($msg)) {
	echo $msg;
}
?>
    <div class="container">
        <div class="row mt-3">
            <div class="col-md-7 mt-5">
                <img src="../img/Logo2.png" width="500" alt=""> 
            </div>
            <div class="col-md-5 mt-5">
                <div class="card shadow p-3 border-radius-20 border-0">
                    <div class="card-body">
                        <h3>Booking Lapangan</h3>
                    <form method="post" name="autoSumForm" class="mt-4" action="">
                        <div class="form-group">
                            <div class="row-auto my-1">
                            <h8>Nama Member</h8>
                            </div>
                            <input type="text" name="nama" class="form-control" value="<?php echo $_SESSION['nama']; ?>" readonly>
                        </div>
                        <div class="form-group">
                            <div class="row-auto my-1">
                            <h7>Tanggal Main</h7>
                            </div>
                            <input type="date" name="tanggal" class="form-control" placeholder="Pilih Tanggal" Required>
                        </div>
                        <div class="form-group">
                            <div class="row-auto my-1">
                            <h7>Jam Mulai</h7>
                            </div>
                            <select class="form-control" name="jam" required>
                                <option value="" disabled selected hidden>Pilih Jam Mulai</option>
                                <option>00:00</option>
                                <option>01:00</option>
                                <option>02:00</option>
                                <option>03:00</option>
                                <option>04:00</option>
                                <option>05:00</option>
                                <option>06:00</option>
                                <option>07:00</option>
                                <option>08:00</option>
                                <option>09:00</option>
                                <option>10:00</option>
                                <option>11:00</option>
                                <option>12:00</option>
                                <option>13:00</option>
                                <option>14:00</option>
                                <option>15:00</option>
                                <option>16:00</option>
                                <option>17:00</option>
                                <option>18:00</option>
                                <option>19:00</option>
                                <option>20:00</option>
                                <option>21:00</option>
                                <option>22:00</option>
                                <option>23:00</option>
                              </select>
                        </div>
                        <div class="form-group">
                            <div class="row-auto my-1">
                            <h7>Durasi Bermain</h7>
                            </div>
                            <select class="form-control" name="durasi" onFocus="startCalc();" onBlur="stopCalc();" required>
                                <option value="" disabled selected hidden>Pilih Durasi</option>
                                <option value="1">1 Jam</option>
                                <option value="2">2 Jam</option>
                                <option value="3">3 Jam</option>
                                <option value="4">4 Jam</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="row-auto my-1">
                            <h7>No Telp</h7>
                            </div>
                            <input type="text" name="no_telp" class="form-control" placeholder="No Telp" Required>
                        </div>
                        <div class="form-group">
                            <div class="row-auto my-1">
                            <h7>Pembayaran Awal</h7>
                            </div>
                            <input type="number" min="20000" name="Status" class="form-control" placeholder="Pembayaran Awal Minimal Rp.20.000" Required>
                        </div>
                        <hr>
                         <div class="form-group">
                            <h6 class="font-weight-bold">Nama Lapangan</h6>
                            <input type="text" name="nama_lapangan" class="form-control" value="<?=$nama_lapangan;?>" readonly>
                        </div>
                        <div class="form-group">
                            <h6 class="font-weight-bold">Harga Per Jam</h6>
                            <input type="text" name="harga_lapangan" onFocus="startCalc();" onBlur="stopCalc();" class="form-control" value="<?=$harga;?>" readonly>
                        </div>
                        <div class="form-group">
                            <h6 class="font-weight-bold">Total Biaya</h6>
                            <input type="text" name="total_biaya" class="form-control" value="0" readonly>
                        </div>
                        <div>
                            <h6>note :</h6>
                            <h6>
                                Kirim bukti pembayaran ke
                                <a href="https://api.whatsapp.com/send/?phone=085866410114&text&type=phone_number&app_absent=0" target="_blank" class="text-decoration-none">sini</a>
                            </h6>
                            <b>*Batas waktu 30 menit setelah melakukan booking</b>
                        </div>
                        <button type="submit" class="btn btn-color-theme btn-block p-2 mt-3" name="btn-save">Booking</button>
                    </form>

                    <!-- JS Total Harga -->
                    <script>
                    function startCalc(){
                        interval = setInterval("calc()",1);}
                        function calc(){
                            one = document.autoSumForm.durasi.value;
                            two = document.autoSumForm.harga_lapangan.value;
                            document.autoSumForm.total_biaya.value = (one * 1) * (two * 1);}
                            function stopCalc(){
                                clearInterval(interval);}
                                </script>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

   <?php } ?>
  </body>
</html>