-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Jan 2023 pada 20.44
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ti3`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_lapangan`
--

CREATE TABLE `tbl_lapangan` (
  `id` int(3) NOT NULL,
  `nama_lapangan` varchar(200) NOT NULL,
  `harga` varchar(200) NOT NULL,
  `foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_lapangan`
--

INSERT INTO `tbl_lapangan` (`id`, `nama_lapangan`, `harga`, `foto`) VALUES
(1, 'Lapangan 1', '100000', 'l1.png'),
(2, 'Lapangan 2', '150000', 'l2.png'),
(3, 'Lapangan 3', '200000', 'l8.png'),
(4, 'Lapangan 4', '250000', 'l3.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `id` int(3) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `nama_lapangan` varchar(255) NOT NULL,
  `tanggal` varchar(200) NOT NULL,
  `jam` varchar(200) NOT NULL,
  `durasi` varchar(200) NOT NULL,
  `no_telp` varchar(200) NOT NULL,
  `harga_lapangan` varchar(255) NOT NULL,
  `total_biaya` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id`, `nama`, `nama_lapangan`, `tanggal`, `jam`, `durasi`, `no_telp`, `harga_lapangan`, `total_biaya`) VALUES
(6, 'Andreas Novito Andi Sano', 'Lapangan 3', '28-01-2023', '13:00', '3', '088888', '200000', '600000'),
(8, 'Hasby Arrahman', 'Lapangan 3', '28-01-2023', '15:00', '2', '081333333', '200000', '400000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(3) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` enum('admin','user') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nama`, `username`, `password`, `email`, `level`) VALUES
(25, 'Alga Alfara', 'alga123', 'f6b43b489adbb64d84a8de1d1a6a7f17', 'alga123@gmail.com', 'admin'),
(28, 'Andreas Novito Andi Sano', 'andreas123', 'b8c311d250851d59be8eb3658c79e049', 'andreas123@gmail.com', 'user'),
(32, 'Hasby Arrahman', 'hasby123', 'ba6d727ebbde56f526786f82ae2a781e', 'hasby123@gmail.com', 'user');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_lapangan`
--
ALTER TABLE `tbl_lapangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_lapangan`
--
ALTER TABLE `tbl_lapangan`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
